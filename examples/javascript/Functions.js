#!/usr/bin/env node

/* jshint
    esversion: 6,
    evil:      true,
    sub:       true
*/

// ------------
// Functions.js
// ------------

"use strict";

const assert = require('assert');
const _      = require('lodash');

function square1 (v) {
    return Math.pow(v, 2);}

function test1 () {
    const a = [2, 3, 4];
    const m = _.map(a, square1);
    assert(_.isEqual(m, [4, 9, 16]));}

const square2 = function (v) {return Math.pow(v, 2);};

function test2 () {
    const a = [2, 3, 4];
    const m = _.map(a, square2);
    assert(_.isEqual(m, [4, 9, 16]));}

const square3 = (v) => {return Math.pow(v, 2);};

function test3 () {
    const a = [2, 3, 4];
    const m = _.map(a, square3);
    assert(_.isEqual(m, [4, 9, 16]));}

function pow1 (p) {
    function f (v) {
        return Math.pow(v, p);}
    return f;}

function test4 () {
    const a = [2, 3, 4];
    const m = _.map(a, pow1(2));
    assert(_.isEqual(m, [4, 9, 16]));}

function pow2 (p) {
    return function (v) {return Math.pow(v, p);};}

function test5 () {
    const a = [2, 3, 4];
    const m = _.map(a, pow2(2));
    assert(_.isEqual(m, [4, 9, 16]));}

function pow3 (p) {
    return (v) => {return Math.pow(v, p);};}

function test6 () {
    const a = [2, 3, 4];
    const m = _.map(a, pow3(2));
    assert(_.isEqual(m, [4, 9, 16]));}

function test7 () {
    const a  = [2, 3, 4];
    const fs = [];
    for (var n = 0; n != 3; ++n)
        fs.push((v) => {return Math.pow(v, n);});
    const ms = [];
    for (let i = 0; i != 3; ++i)
        ms.push(_.map(a, fs[i]));
    assert(_.isEqual(ms, [[8, 27, 64], [8, 27, 64], [8, 27, 64]]));}

function test8 () {
    const a  = [2, 3, 4];
    const fs = [];
    for (let n = 0; n != 3; ++n)
        fs.push((v) => {return Math.pow(v, n);});
    const ms = [];
    for (let i = 0; i != 3; ++i)
        ms.push(_.map(a, fs[i]));
    assert(_.isEqual(ms, [[1, 1, 1], [2, 3, 4], [4, 9, 16]]));}

function A () {
    this.v = 2;}

function test9 () {
//  const x = A();         // TypeError: Cannot set property 'v' of undefined
    const x = new A();
    assert(x.v    == 2);
    assert(x["v"] == 2);}

function main () {
    console.log("Functions.js");
    for (let i of _.range(9))
        eval("test" + (i + 1) + "()");
    console.log("Done.");}

main();
