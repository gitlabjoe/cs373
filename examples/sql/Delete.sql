-- ----------
-- Delete.sql
-- ----------

use test;

-- -----------------------------------------------------------------------
select "*** drop table Apply ***";
drop table if exists Apply;

-- -----------------------------------------------------------------------
select "*** drop table College ***";
drop table if exists College;

-- -----------------------------------------------------------------------
select "*** drop table Student ***";
drop table if exists Student;

-- -----------------------------------------------------------------------
select "*** create table Student ***";
create table Student (
    sID         int not null,
    sName       text,
    GPA         float,
    sizeHS      int,
    primary key (sID));

-- -----------------------------------------------------------------------
select "*** create table College ***";
create table College (
    cName       varchar(8) not null,
    state       char(2),
    enrollment  int,
    primary key (cName));

-- -----------------------------------------------------------------------
select "*** create table Apply ***";
create table Apply (
    sID         int,
    cName       varchar(8),
    major       text,
    decision    boolean);

-- -----------------------------------------------------------------------
select "*** insert Student ***";
insert into Student values (123, "Amy",    3.9,  1000);
insert into Student values (234, "Bob",    3.6,  1500);
insert into Student values (320, "Lori",   null, 2500);
insert into Student values (321, "Mary",   2.5,  1200);
insert into Student values (345, "Craig",  3.5,   500);
insert into Student values (432, "Kevin",  null, 1500);
insert into Student values (456, "Doris",  3.9,  1000);
insert into Student values (543, "Craig",  3.4,  2000);
insert into Student values (567, "Edward", 2.9,  2000);
insert into Student values (654, "Amy",    3.9,  1000);
insert into Student values (678, "Fay",    3.8,   200);
insert into Student values (765, "Jay",    2.9,  1500);
insert into Student values (789, "Gary",   3.4,   800);
insert into Student values (876, "Irene",  3.9,   400);
insert into Student values (987, "Helen",  3.7,   800);

-- -----------------------------------------------------------------------
select "*** insert College ***";
insert into College values ("A&M",      "TX", 25000);
insert into College values ("Berkeley", "CA", 36000);
insert into College values ("Cornell",  "NY", 21000);
insert into College values ("MIT",      "MA", 10000);
insert into College values ("Stanford", "CA", 15000);
insert into College values ("UCF",      "FL", 36000);

-- -----------------------------------------------------------------------
select "*** insert Apply ***";
insert into Apply values (123, "Berkeley", "CS",             true);
insert into Apply values (123, "Cornell",  "EE",             true);
insert into Apply values (123, "Stanford", "CS",             true);
insert into Apply values (123, "Stanford", "EE",             false);
insert into Apply values (234, "Berkeley", "biology",        false);
insert into Apply values (321, "MIT",      "history",        false);
insert into Apply values (321, "MIT",      "psychology",     true);
insert into Apply values (345, "Cornell",  "bioengineering", false);
insert into Apply values (345, "Cornell",  "CS",             true);
insert into Apply values (345, "Cornell",  "EE",             false);
insert into Apply values (345, "MIT",      "bioengineering", true);
insert into Apply values (543, "MIT",      "CS",             false);
insert into Apply values (678, "Stanford", "history",        true);
insert into Apply values (765, "Cornell",  "history",        false);
insert into Apply values (765, "Cornell",  "psychology",     true);
insert into Apply values (765, "Stanford", "history",        true);
insert into Apply values (876, "MIT",      "biology",        true);
insert into Apply values (876, "MIT",      "marine biology", false);
insert into Apply values (876, "Stanford", "CS",             false);
insert into Apply values (987, "Berkeley", "CS",             true);
insert into Apply values (987, "Stanford", "CS",             true);

-- -----------------------------------------------------------------------
select "*** show Student ***";
select * from Student;

-- -----------------------------------------------------------------------
select "*** show College ***";
select * from College;

-- -----------------------------------------------------------------------
select "*** show Apply ***";
select * from Apply;

-- ------------------------------------------------------------------------
-- students who applied to two or more majors

select "*** query #1a ***";
select sID, count(distinct major)
    from Apply
    group by sID
    having count(distinct major) > 2;

-- ------------------------------------------------------------------------
-- all students

select "*** query #1b ***";
select count(*) from Student;
select *        from Student;

-- ------------------------------------------------------------------------
-- delete those students from Student
-- why is this dangerous?

select "*** query #1c ***";
delete
    from Student
    where sID in
        (select sID
            from Apply
            group by sID
            having count(distinct major) > 2);

-- ------------------------------------------------------------------------
-- all students

select "*** query #1d ***";
select count(*) from Student;
select *        from Student;

-- ------------------------------------------------------------------------
-- delete those students from Apply

/*
this does not work in MySQL
can not delete from a relation that is in a subquery

delete
    from Apply
    where sID in
        (select sID
            from Apply
            group by sID
            having count(distinct major) > 2);

create temporary table first
*/

-- ------------------------------------------------------------------------
-- all applications

select "*** query #2a ***";
select count(*) from Apply;
select *        from Apply;

-- ------------------------------------------------------------------------
-- delete those students from Apply

select "*** query #2b ***";
create temporary table T as
    select sID
        from Apply
        group by sID
        having count(distinct major) > 2;

select "*** query #2c ***";
select count(*) from T;
select *        from T;

select "*** query #2d ***";
select count(*)
    from Apply
    where sID in
        (select *
            from T);

select *
    from Apply
    where sID in
        (select *
            from T);

select "*** query #2e ***";
delete
    from Apply
    where sID in
        (select *
            from T);

-- ------------------------------------------------------------------------
-- all applications

select "*** query #2f ***";
select count(*) from Apply;
select *        from Apply;

-- ------------------------------------------------------------------------
-- colleges without CS applications

select "*** query #3a ***";
select count(*)
    from College
    where cName not in
        (select cName
            from Apply
            where major = 'CS');

select cName
    from College
    where cName not in
        (select cName
            from Apply
            where major = 'CS');

-- ------------------------------------------------------------------------
-- all colleges

select "*** query #3b ***";
select count(*) from College;
select *        from College;

-- ------------------------------------------------------------------------
-- delete those colleges from College
-- why is this dangerous?

select "*** query #3c ***";
delete
    from College
    where cName not in
        (select cName
            from Apply
            where major = 'CS');

-- ------------------------------------------------------------------------
-- all colleges

select "*** query #3d ***";
select count(*) from College;
select *        from College;

-- ------------------------------------------------------------------------
drop table if exists Student;
drop table if exists Apply;
drop table if exists College;

exit
