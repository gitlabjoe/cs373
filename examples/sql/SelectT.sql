-- -----------
-- SelectT.sql
-- -----------

use test;

-- -----------------------------------------------------------------------
-- student ID, school name, and major of
-- applications that were accepted
-- sorted by school in ascending order

select "*** query #1 ***";
select sID, cName, major
    from Apply
    where decision is true
    order by cName;

-- -----------------------------------------------------------------------
-- distinct school name and decision of
-- applications to CS that were accepted
-- sorted by school in descending order
-- limited to two results

select "*** query #2 ***";
select distinct cName, decision
    from Apply
    where major = "CS" and decision is true
    order by cName desc
    limit 2;

-- -----------------------------------------------------------------------
-- student name and high school size of
-- students whose names end in "y"

select "*** query #3 ***";
select sName, sizeHS
    from Student
    where sName like "%y";

-- -----------------------------------------------------------------------
-- min, max, and average high school size of
-- students whose names have three letters and end in "y"

select "*** query #4 ***";
select min(sizeHS), max(sizeHS), avg(sizeHS)
    from Student
    where sName like "__y";

-- -----------------------------------------------------------------------
-- number of schools in CA or TX
-- MUST USE in

select "*** query #5 ***";
select count(*)
    from College
    where state in ("CA", "TX");

-- -----------------------------------------------------------------------
-- state, min, max, and avg enrollment of
-- schools whose enrollment is between 20000 and 30000
-- for each state
-- MUST USE between

select "*** query #6 ***";
select state, min(enrollment), max(enrollment), avg(enrollment)
    from College
    where enrollment between 20000 and 30000
    group by state;

-- -----------------------------------------------------------------------
-- state, min, max, and average enrollment of
-- schools whose min enrollment is between 15000 and 25000
-- for each state
-- MUST USE having

select "*** query #7 ***";
select state, min(enrollment), max(enrollment), avg(enrollment)
    from College
    group by state
    having min(enrollment) between 15000 and 25000;

exit
