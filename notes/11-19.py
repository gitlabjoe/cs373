# -----------
# Mon, 19 Nov
# -----------

"""
Replace Type Code with Subclasses

changing Movie to an abstract class and creating
    NewMovie
    ChildrensMovie
    RegularMovie

we'd have to change the tests
we wouldn't need a price code at all, the switch either
difficult to change type of movie
"""

"""
relationships that classes have
    inheritance
    aggregation, association, composition
"""

"""
Replace Type Code with State / Strategy

create an abstract Price class and
    NewPrice
    RegularPrice
    ChildrensPrice

don't have to the change tests
changing type is easy
switch doesn't completely go away, but runs much less often
"""
