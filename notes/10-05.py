# -----------
# Fri,  5 Oct
# -----------

for v in a : # a has to be iterable
    ...

for u, v in a : # a has to be iterable of iterables of len 2
    ...

a = [item, item, ...] # list
a = [<code>]          # list

a = (item, item, ...) # tuple
a = (<code>)          # NOT a tuple, a generator

"""
range  is a lazy container
count  is a lazy generator (iterator)
zip    is a lazy generator (iterator)
map    is a lazy generator (iterator)
filter is a lazy generator (iterator)
"""

"""
zip takes any number of iterables and generates tuples

map takes a unary function and an iterable and generates the result of calling that function on the elements of the iterable

filter takes a unary predicate and an iterable and generates the elements of the iterable that satisfy the predicate
"""
