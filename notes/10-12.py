# -----------
# Fri, 12 Oct
# -----------

"""
=, *, **
function definition: =, *(2), **
function call: =, *, **
"""

def f (*t, **d) :
    ...

def g (*, x, *t) :
