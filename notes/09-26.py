# -----------
# Wed, 26 Sep
# -----------

"""
Factorial
1. simple solution
2. use reduce and mul
"""

"""
arguments to reduce
1. a binary function
2. an iterable
3. a value, a seed
"""

"""
s @ a1 @ a2 @ a3 @ ... @ an-1

s = 0
@ = +
sum

s = 1
@ = *
product

s = 1
@ = *
values are 1 to n
factorial
"""

def mul (x, y) :
    return x * y

for v in a :
    ...

"""
a has to be iterable
"""

"""
indexables
list, tuple, str

iterables that are not indexalbes
dict, set, frozenset
"""

a = [2, 3, 4]
print(type(a)) # list
p = iter(a)
print(type(p)) # list iterator
v = next(p)
print(v)       # 2
v = next(p)
print(v)       # 3
q = iter(a)
w = next(q)
print(w)       # 2

v = next(p)
print(v)       # 4
v = next(p)    # raise StopIteration

s = {2, 3, 4}
print(type(s)) # set
p = iter(a)
print(type(p)) # set iterator
v = next(p)
print(v)       # 2, maybe
v = next(p)
print(v)       # 3, maybe
v = next(p)
print(v)       # 4, maybe
v = next(p)    # raise StopIteration
p = iter(s)
print(type(p)) # set iterator
